<?php



class Students{

    public function getStudent(string $Id){
        try {
            $name = 'Estudiantes';
            if (empty($Id)) throw new Exception('Document name missing');
            if ($this->db->collection($this->name)->document($Id)->snapshot()->exists()) {
                return $this->db->collection($this->name)->document($Id)->snapshot()->data();
            } else {
                throw new Exception('Document are not exists');
            }
        } catch (Exception $exception) {
            return $exception->getMessage();
        }
    }

}